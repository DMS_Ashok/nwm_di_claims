package com.nwm.claims.businessrules.utility;

import ilog.rules.bom.annotations.BusinessType;

import ilog.rules.bom.annotations.CollectionDomain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.nwm.claims.businessrules.di.fasttrack.model.Policy;
import com.nwm.claims.businessrules.di.fasttrack.model.SpecialtyPrioritization;

public class CommonUtility {
	/*@CollectionDomain(elementType = "VirtualDomains.MedicalEvidenceSpecialty")
	public static List<String> MDSASpecalityList=new ArrayList<String>();
	@CollectionDomain(elementType = "VirtualDomains.MedicalEvidenceSpecialty")
	public static List<String> generalSpecalityList=new ArrayList<String>();*/
	/*
	 * public static void main(String[] args) { Map<Integer,String> map1=new
	 * TreeMap<>(); map1.put(2, "Oncology"); map1.put(3, "Hematology");
	 * map1.put(1, "neurology"); map1.put(1, "neurology1"); map1.put(5,
	 * "neurology5"); map1.put(4, "neurology4"); System.out.println(map1);
	 * Collection<String> l1=new ArrayList<String>(); l1=map1.values();
	 * System.out.println(l1); }
	 */

	/*public static int getSizeOf(List<Object> aList) {
		if (aList != null)
			return aList.size();
		return 0;
	}*/
	/*public static String asString(Object object) {
		String value = "";
		if (object == null)
			return value;
		value = (String) object.toString();
		return value;
	}
*/
	public static int getSizeOf(List<?> aList) {

		if (aList != null)
			return aList.size();
		return 0;
	}
	public static void addGeneralSpecialties(@CollectionDomain(elementType = "VirtualDomains.MedicalEvidenceSpecialty") List<String> generalSpecialties, @CollectionDomain(elementType = "VirtualDomains.MedicalEvidenceSpecialty") List<String> specialtyList){
		if(generalSpecialties!=null && specialtyList!=null){
			Set<String> uniqueSpecialties=new LinkedHashSet<String>();
			uniqueSpecialties.addAll(generalSpecialties);
			specialtyList.clear();
			specialtyList.addAll(uniqueSpecialties);
		}
	}
	public static boolean testIdentityMatchConcern(@BusinessType("VirtualDomains.MedicalRedFlags") String redFlagStatus, @CollectionDomain(elementType = "VirtualDomains.MedicalRedFlags") List<String> medicalRedFlagList){
		if(redFlagStatus!=null && !redFlagStatus.isEmpty() && medicalRedFlagList!=null && !medicalRedFlagList.isEmpty()){
			if(medicalRedFlagList.contains(redFlagStatus))
			return true;
			
		}
		return false;
	}
	public static boolean fastTrackDocStatusTest(@BusinessType("VirtualDomains.FastTrakReqDocInGoodOrder") String redFlagStatus, @CollectionDomain(elementType = "VirtualDomains.FastTrakReqDocInGoodOrder") List<String> medicalRedFlagList){
		if(redFlagStatus!=null && !redFlagStatus.isEmpty() && medicalRedFlagList!=null && !medicalRedFlagList.isEmpty()){
			if(medicalRedFlagList.contains(redFlagStatus))
			return true;
			
		}
		return false;
	}
	
	/*public static boolean isNotInGeneralSpecialties(@BusinessType("VirtualDomains.MedicalEvidenceSpecialty") String specialty){
		if(!MDSASpecalityList.isEmpty() && !generalSpecalityList.isEmpty() && specialty!=null){
			if(MDSASpecalityList.contains(specialty) || generalSpecalityList.contains(specialty)){
				return false;
			}
		}
		return true;
	}*/
	
	/*public static boolean fastTrackRedFlagRequiredDocsStatusCheck(@BusinessType("VirtualDomains.FastTrakReqDocInGoodOrder") String docStatus, @CollectionDomain(elementType = "VirtualDomains.FastTrakReqDocInGoodOrder") List<String> redFlagList){
		boolean flag=false;
		if(docStatus!=null && redFlagList!=null && !docStatus.isEmpty() && !redFlagList.isEmpty()){
			for(String tempStatus: redFlagList){
				if(tempStatus!=null && !tempStatus.isEmpty()){
					if(tempStatus.equals(docStatus)){
						flag=true;
					}
					else if(!tempStatus.equals(docStatus)){
						flag=false;
					}
				}
				else
					flag=false;
			}
		}
		return flag;
	}*/

	public static void removeDuplicateObjects(List<SpecialtyPrioritization> specialtyPrioritizations) {
		Set<SpecialtyPrioritization> set = new TreeSet<SpecialtyPrioritization>(new Comparator<SpecialtyPrioritization>() {
			@Override
			public int compare(SpecialtyPrioritization sp1, SpecialtyPrioritization sp2) {
				if(sp1.equals(sp2)){
	        		return 0;
	        	}
	        	return 1;
			}
		});
		set.addAll(specialtyPrioritizations);

	    final ArrayList<SpecialtyPrioritization> tempSpecialtyPrioritizations = new ArrayList<SpecialtyPrioritization>(set);

	    /** Printing original list **/
	    specialtyPrioritizations.clear();
	    specialtyPrioritizations.addAll(tempSpecialtyPrioritizations);
	    //specialtyPrioritizations=tempSpecialtyPrioritizations;
		//System.out.println(tempSpecialtyPrioritizations.size());
	}
	/*public static void removeDuplicateObjects(List<SpecialtyPrioritization> specialtyPrioritizations) {
		if (specialtyPrioritizations != null && !specialtyPrioritizations.isEmpty()) {
			Set<SpecialtyPrioritization> tempSpecialtyPrioritizations = new HashSet<SpecialtyPrioritization>();
			for (SpecialtyPrioritization specialtyPrioritization : specialtyPrioritizations) {

				if (specialtyPrioritization != null) {
					if (tempSpecialtyPrioritizations.isEmpty()) {
						tempSpecialtyPrioritizations.add(specialtyPrioritization);
					} else {
						Object objTemp[]=tempSpecialtyPrioritizations.toArray();
						SpecialtyPrioritization spTemp[]=new SpecialtyPrioritization[objTemp.length];
						for(int i=0;i<objTemp.length;i++){
							spTemp[i]=(SpecialtyPrioritization)objTemp[i];
						}
						
						Iterator<SpecialtyPrioritization> iterator=tempSpecialtyPrioritizations.iterator();
						int priority=0;
						String impairmentCode="";
						String evidenceSpeciality="";
						int cnt=0;
						while(iterator.hasNext())
						{
							if(cnt==0)
							{
								SpecialtyPrioritization sp= iterator.next();
								priority=sp.getPriority();
								impairmentCode=sp.getImpairmentCode();
								evidenceSpeciality=sp.getEvidenceSpecialty();
							}
							else
							{
								SpecialtyPrioritization spp=iterator.next();
								if(priority==spp.getPriority() && impairmentCode.equals(spp.getImpairmentCode()) && evidenceSpeciality.equals(spp.getEvidenceSpecialty()))
									iterator.remove();
							}
						}
						for (SpecialtyPrioritization tempSpecialtyPrioritization : spTemp) {
							if (specialtyPrioritization.getImpairmentCode() != null
									&& !specialtyPrioritization.getImpairmentCode().isEmpty()
									&& specialtyPrioritization.getEvidenceSpecialty() != null
									&& !specialtyPrioritization.getEvidenceSpecialty().isEmpty()
									&& tempSpecialtyPrioritization.getImpairmentCode() != null
									&& !tempSpecialtyPrioritization.getImpairmentCode().isEmpty()
									&& tempSpecialtyPrioritization.getEvidenceSpecialty() != null
									&& !tempSpecialtyPrioritization.getEvidenceSpecialty().isEmpty()) {
								if (specialtyPrioritization.getPriority() != tempSpecialtyPrioritization.getPriority()
										&& !specialtyPrioritization.getImpairmentCode().equals(
												tempSpecialtyPrioritization.getImpairmentCode())
										&& !specialtyPrioritization.getEvidenceSpecialty().equals(
												tempSpecialtyPrioritization.getEvidenceSpecialty())) {
                                  tempSpecialtyPrioritizations.add(tempSpecialtyPrioritization);
                                  System.out.println(tempSpecialtyPrioritization.getImpairmentCode()+" code "+temp);
								}

							}
						}
					}
					System.out.println(tempSpecialtyPrioritizations.size()+" ggg");
					
				}
			}
			specialtyPrioritizations.clear();
			specialtyPrioritizations.addAll(tempSpecialtyPrioritizations);
		}
		System.out.println(specialtyPrioritizations.size()+" splenth");
		for(SpecialtyPrioritization sps1: specialtyPrioritizations){
			System.out.println(sps1.getEvidenceSpecialty());
			System.out.println(sps1.getPriority());
			System.out.println(sps1.getImpairmentCode());
		}
	}*/

	/*public static void main(String[] args) {
		SpecialtyPrioritization sp1=new SpecialtyPrioritization();
		sp1.setImpairmentCode("100");
		sp1.setEvidenceSpecialty("abc");
		sp1.setPriority(1);
		SpecialtyPrioritization sp2=new SpecialtyPrioritization();
		sp2.setImpairmentCode("101");
		sp2.setEvidenceSpecialty("abc1");
		sp2.setPriority(2);
		SpecialtyPrioritization sp3=new SpecialtyPrioritization();
		sp3.setImpairmentCode("102");
		sp3.setEvidenceSpecialty("abc2");
		sp3.setPriority(3);
		SpecialtyPrioritization sp4=new SpecialtyPrioritization();
		sp4.setImpairmentCode("103");
		sp4.setEvidenceSpecialty("abc");
		sp4.setPriority(4);
		SpecialtyPrioritization sp5=new SpecialtyPrioritization();
		sp5.setImpairmentCode("103");
		sp5.setEvidenceSpecialty("abc");
		sp5.setPriority(4);
		SpecialtyPrioritization sp6=new SpecialtyPrioritization();
		sp6.setImpairmentCode("103");
		sp6.setEvidenceSpecialty("abc2");
		sp6.setPriority(4);
		SpecialtyPrioritization sp7=new SpecialtyPrioritization();
		sp7.setImpairmentCode("103");
		sp7.setEvidenceSpecialty("abc1");
		sp7.setPriority(4);
		List<SpecialtyPrioritization> sps=new ArrayList<SpecialtyPrioritization>();
		sps.add(sp1);
		sps.add(sp2);
		sps.add(sp3);
		sps.add(sp4);
		sps.add(sp5);
		sps.add(sp6);
		sps.add(sp7);
		
		removeDuplicateObjects(sps);
		
		
	}*/
	public static void addMedicalRecordsDetails(SpecialtyPrioritization specialtyPrioritization,
			List<SpecialtyPrioritization> specialtyPrioritizationList) {
		if (specialtyPrioritization != null && specialtyPrioritizationList != null) {
			specialtyPrioritizationList.add(specialtyPrioritization);
		}
	}

	public static void getSortedList(List<SpecialtyPrioritization> specialtyPrioritizationList) {
		if (specialtyPrioritizationList != null && !specialtyPrioritizationList.isEmpty())
			Collections.sort(specialtyPrioritizationList, SpecialtyPrioritization.priorityComparator);
	}

	public static SpecialtyPrioritization getSpecialtyPrioritization(String impairmentCode, int priority,
			@BusinessType("VirtualDomains.MedicalEvidenceSpecialty") String specialty) {
		SpecialtyPrioritization specialtyPrioritization = new SpecialtyPrioritization();
		specialtyPrioritization.setImpairmentCode(impairmentCode);
		specialtyPrioritization.setPriority(priority);
		specialtyPrioritization.setEvidenceSpecialty(specialty);
		return specialtyPrioritization;
	}

	public static @CollectionDomain(elementType = "VirtualDomains.MedicalEvidenceSpecialty") List<String> getPrioritizedSpecialties(
			List<SpecialtyPrioritization> specialtyPrioritizationList) {
		List<String> prioritizedSpecialties = new ArrayList<String>();
		if (specialtyPrioritizationList != null && !specialtyPrioritizationList.isEmpty()) {
			for (SpecialtyPrioritization specialtyPrioritization : specialtyPrioritizationList) {
				if (specialtyPrioritization != null && specialtyPrioritization.getEvidenceSpecialty() != null
						&& !specialtyPrioritization.getEvidenceSpecialty().isEmpty()) {
					prioritizedSpecialties.add(specialtyPrioritization.getEvidenceSpecialty());
				}
			}
		}
		return prioritizedSpecialties;
	}

	public static boolean isMDSASpecialtyListEmpty(
			@CollectionDomain(elementType = "VirtualDomains.MedicalEvidenceSpecialty") List<String> MDSAList) {
		if (MDSAList == null || MDSAList.isEmpty())
			return true;
		else
			return false;
	}

	public static boolean isEmpty(List<String> MDSACodes) {
		if (MDSACodes == null || MDSACodes.isEmpty())
			return true;
		else
			return false;
	}
	public static boolean fastTrackDocStatusIsEmpty(@CollectionDomain(elementType="VirtualDomains.FastTrakReqDocInGoodOrder") List<String> docStatusList) {
		if (docStatusList == null || docStatusList.isEmpty())
			return true;
		else
			return false;
	}
	/*public static void clear(@CollectionDomain(elementType = "VirtualDomains.MedicalEvidenceSpecialty") List<String> additionRecords) {
		if (additionRecords != null && !additionRecords.isEmpty())
			additionRecords.clear();
	}*/

	public static void addBusinessPersonalRedFlag(
			@CollectionDomain(elementType = "VirtualDomains.BusinessPersonalRedFlag") List<String> businessPersonalRedFlagList,
			@BusinessType("VirtualDomains.BusinessPersonalRedFlag") String businessPersonalRedFlag) {
		if (businessPersonalRedFlagList != null && businessPersonalRedFlag != null)
			businessPersonalRedFlagList.add(businessPersonalRedFlag);
	}
	/*public static void addFastTrackDocStatus(
			@CollectionDomain(elementType="VirtualDomains.FastTrakReqDocInGoodOrder") List<String> fastTrackDocStatusList,
			@BusinessType("VirtualDomains.FastTrakReqDocInGoodOrder") String documentStatus) {
		if (fastTrackDocStatusList != null && documentStatus != null)
			fastTrackDocStatusList.add(documentStatus);
	}*/
	public static void addAdditionalInformationRedFlag(List<String> additionalInfoRedFlagList, String addInfoRedFlag) {
		if (additionalInfoRedFlagList != null && addInfoRedFlag != null)
			additionalInfoRedFlagList.add(addInfoRedFlag);
	}

	/*public static void addMedicalRecordsMatchConcern(
			@CollectionDomain(elementType = "VirtualDomains.MedicalRedFlags") List<String> medRecMatchConcernList,
			@BusinessType("VirtualDomains.MedicalRedFlags") String medRecMatchConcern) {
		if (medRecMatchConcernList != null && medRecMatchConcern != null)
			medRecMatchConcernList.add(medRecMatchConcern);

	}*/

	public static void addImpairmentSpecialty(
			@CollectionDomain(elementType = "VirtualDomains.MedicalEvidenceSpecialty") List<String> impairmentSpecialtyList,
			@BusinessType("VirtualDomains.MedicalEvidenceSpecialty") String impairmentSpecialtyValue) {
		if (impairmentSpecialtyList != null && impairmentSpecialtyValue != null)
			impairmentSpecialtyList.add(impairmentSpecialtyValue);
	}

	public static void addSecondaryImpairmentsMDSAList(List<String> secondaryMDSAImpairmentCodes, String impairmentCode) {
		if (impairmentCode != null) {
			if (secondaryMDSAImpairmentCodes == null)
				secondaryMDSAImpairmentCodes = new ArrayList<String>();
			secondaryMDSAImpairmentCodes.add(impairmentCode);
			// System.out.println(secondaryMDSAImpairmentCodes);
		}
	}

	public static void addApplicableEvidenceSpecialty(
			@CollectionDomain(elementType = "VirtualDomains.MedicalEvidenceSpecialty") List<String> applicableSpecialtyList,
			@BusinessType("VirtualDomains.MedicalEvidenceSpecialty") String medicalEvidenceSpecialty) {
		if (applicableSpecialtyList != null && medicalEvidenceSpecialty != null
				&& !applicableSpecialtyList.contains(medicalEvidenceSpecialty))
			applicableSpecialtyList.add(medicalEvidenceSpecialty);
	}

	public static void getMedicalSpecialtyCandidateList(
			@CollectionDomain(elementType = "VirtualDomains.MedicalEvidenceSpecialty") List<String> medicalSpecialtyCandidateList,
			@CollectionDomain(elementType = "VirtualDomains.MedicalEvidenceSpecialty") List<String> appliableSpecialtyList) {
		if (medicalSpecialtyCandidateList != null && appliableSpecialtyList != null) {
			if (!medicalSpecialtyCandidateList.containsAll(appliableSpecialtyList))
				medicalSpecialtyCandidateList.addAll(appliableSpecialtyList);
		}
	}

	
	public static void getAdditionalSpecialtyList(
			@CollectionDomain(elementType = "VirtualDomains.MedicalEvidenceSpecialty") List<String> medicalSpecialtyCandidateList,
			@CollectionDomain(elementType = "VirtualDomains.MedicalEvidenceSpecialty") List<String> treatmentDateAppliableSpecialtyList) {
		
		List<String> tempMedicalRecordsList = new ArrayList<String>();
		// Set<String> uniqueMedicalSpecialtyCandidateList = new TreeSet<>();
		if (medicalSpecialtyCandidateList != null && treatmentDateAppliableSpecialtyList != null) {
			tempMedicalRecordsList.addAll(medicalSpecialtyCandidateList);
			for (String tempSpecialty : treatmentDateAppliableSpecialtyList) {
				int cnt = 0;
				Iterator<String> it = tempMedicalRecordsList.iterator();
				while (it.hasNext()) {
					if (tempSpecialty.equals(it.next())) {
						if (cnt == 0) {
							it.remove();
							cnt++;
						} else
							break;
						
					}
				}
			}
			medicalSpecialtyCandidateList.clear();
			medicalSpecialtyCandidateList.addAll(tempMedicalRecordsList);
		}
	
	}
	

	/*
	 * public static void main(String[] args) { List<String> l1=new
	 * ArrayList<String>(); l1.add("1"); l1.add("2"); l1.add("3"); l1.add("4");
	 * l1.add("1"); l1.add("2"); l1.add("1"); l1.add("2"); l1.add("1");
	 * l1.add("2"); l1.add("1"); l1.add("2"); l1.add("1"); l1.add("2");
	 * List<String> l2=new ArrayList<String>(); l2.add("1"); l2.add("2");
	 * l2.add("1"); l2.add("2"); l2.add("1"); l2.add("2"); l2.add("3");
	 * List<String> l3=new ArrayList<String>(); l3.add("1"); l3.add("2");
	 * List<String> l4=new ArrayList<String>(); l4.addAll(l1); List<String>
	 * l5=new ArrayList<String>(); for(String no:l2) { int cnt=0;
	 * Iterator<String> it=l1.iterator(); while(it.hasNext()) {
	 * if(no.equals(it.next())) { if(cnt==0) { it.remove(); cnt++; } else break;
	 * 
	 * 
	 * } else{ System.out.println(no+" no1");
	 * 
	 * 
	 * 
	 * } } } System.out.println(l1); l5=l1; l1=l4;
	 * System.out.println(l1+" lb "+l2+" jkg "+l4+" st "+l5); }
	 */
	/*
	 * public static void main(String[] args) { List<String> l1=new
	 * ArrayList<String>(); l1.add("1"); l1.add("2"); l1.add("1"); Set<String>
	 * s=new TreeSet<>(); List<String> l2=new ArrayList<String>(); l2.add("2");
	 * List<String> l3=new ArrayList<String>(); l3.addAll(l1); l3.removeAll(l2);
	 * List<String> l4=new ArrayList<String>(); List<String> l5=new
	 * ArrayList<String>(); l5.add("2"); l5.add("5");
	 * System.out.println(l1.containsAll(l5)); l4=l3; s.addAll(l1); l1.clear();
	 * l1.addAll(s); System.out.println(l1.contains("1"));
	 * System.out.println(l1+" dh "+l3+" tq "+l4+"st "+l2); }
	 */


	public static void addStrightforwrdMed(List<String> straightForwardMedConditionList, String straightForwardMedCondition) {
		if (straightForwardMedConditionList != null && straightForwardMedCondition != null)
			straightForwardMedConditionList.add(straightForwardMedCondition);

	}

	public static void addClaimDocRedFlag(
			@CollectionDomain(elementType = "VirtualDomains.ClaimDocRedFlags") List<String> claimDocRedFlagList,
			@BusinessType("VirtualDomains.ClaimDocRedFlags") String claimDocRedFlag) {
		if (claimDocRedFlagList != null && claimDocRedFlag != null)

			claimDocRedFlagList.add(claimDocRedFlag);
		System.out.println(claimDocRedFlagList);

	}

	public static void addFastTrackRedFlag(
			@CollectionDomain(elementType = "VirtualDomains.FastTrackRedFlags") List<String> fastTrackRedFlagList,
			@BusinessType("VirtualDomains.FastTrackRedFlags") String fastTrackRedFlag) {
		if (fastTrackRedFlagList != null && fastTrackRedFlag != null)

			fastTrackRedFlagList.add(fastTrackRedFlag);

	}

	public static boolean containsOneOf(List<String> fastTrackRedFlagList, List<String> redFlagsFromTable) {
		if (fastTrackRedFlagList != null && redFlagsFromTable != null) {
			/*
			 * System.out.println(fastTrackRedFlagList + " fast track list " +
			 * redFlagsFromTable);
			 */
			if (fastTrackRedFlagList.containsAll(redFlagsFromTable)) {
				return true;
			} else {
				for (String redFlag : redFlagsFromTable) {
					if (fastTrackRedFlagList.contains(redFlag)) {
						return true;

					}

				}
			}
		}

		return false;
	}

	public static boolean containsAll(List<String> straightForwardList, List<String> secondaryImpairmentList) {
		if (straightForwardList != null && !straightForwardList.isEmpty() && secondaryImpairmentList != null
				&& !secondaryImpairmentList.isEmpty()) {

			if (straightForwardList.containsAll(secondaryImpairmentList)) {
				return true;
			}
		}

		return false;
	}

	/*
	 * public static void main(String[] args) { List<String> l1=new
	 * ArrayList<String>(); l1.add("1"); l1.add("2"); List<String> l2=new
	 * ArrayList<String>(); l2.add("1"); l2.add("2"); l2.add("3"); l1.clear();
	 * System.out.println(l1); }
	 */
	public static void addMedicalRedFlag(@CollectionDomain(elementType = "VirtualDomains.MedicalRedFlags") List<String> medicalRedFlagList,
			@BusinessType("VirtualDomains.MedicalRedFlags") String medicalRedFlag) {
		if (medicalRedFlagList != null && medicalRedFlag != null)

			medicalRedFlagList.add(medicalRedFlag);

	}


	/*public static void addRegularPriority(List<Integer> regularPriority, Integer priority) {
		if (regularPriority != null && priority != null)
			regularPriority.add(priority);
	}

	public static void addMDSAPriority(List<Integer> mdsaPriority, Integer priority) {
		if (mdsaPriority != null && priority != null)
			mdsaPriority.add(priority);
	}

	public static void addPsychologistPriority(List<Integer> psychologistPriority, Integer priority) {
		if (psychologistPriority != null && priority != null)
			psychologistPriority.add(priority);
	}

	public static boolean stringComparision(String str1, String str2) {
		if (str1 == null && str2 == null)
			return false;

		return str1.equals(str2);
	}

	public static Integer getLowestPriority(List<Integer> priorityList) {
		Integer lowestPriority = 0;
		if (priorityList != null && priorityList.size() > 0)
			lowestPriority = Collections.min(priorityList);

		return lowestPriority;
	}*/

	public static boolean isHavingDIPolicy(List<Policy> policies) {
		boolean hasDIPolicy = false;
		if (policies != null && policies.size() > 0) {
			if (policies.size() == 1) {
				hasDIPolicy = policies.get(0).isDIPolicy();
			} else if (policies.size() > 1) {
				lable: for (Policy policy : policies) {
					if (policy.isDIPolicy()) {
						hasDIPolicy = true;
						break lable;
					}
				}
			}
		}
		
		return hasDIPolicy;
	}

	/*
	 * public static void addApplicableMedicalEvidence(Decision response,
	 * MedicalEvidence medicalEvidence) { boolean flag = false; if (null !=
	 * response && null != medicalEvidence) { if
	 * (response.getApplicableMedicalEvidenceList() != null) { lable: for
	 * (MedicalEvidence tempMedEvidence :
	 * response.getApplicableMedicalEvidenceList()) { if (tempMedEvidence !=
	 * null && tempMedEvidence.getMedicalEvidenceID().trim() ==
	 * medicalEvidence.getMedicalEvidenceID().trim()) { flag = true; break
	 * lable; } } if (flag != true) {
	 * response.getApplicableMedicalEvidenceList().add(medicalEvidence); }
	 * 
	 * } } }
	 */

	public static void addGeneralSpecialtyIntoImpairmentList(
			@CollectionDomain(elementType = "VirtualDomains.MedicalEvidenceSpecialty") List<String> generalSpecialtyList,
			@CollectionDomain(elementType = "VirtualDomains.MedicalEvidenceSpecialty") List<String> impairmentsList) {
		if (generalSpecialtyList != null && impairmentsList != null) {
			impairmentsList.addAll(generalSpecialtyList);
		}
	}

	/*
	 * public static void main(String[] args) { List l=new
	 * ArrayList<ApsRequest>(); ApsRequest apsReq=new ApsRequest(); l.add(1);
	 * l.add(2); l.add(3); l.add(4); l.add(5); l.add(6); List l1=new
	 * ArrayList<Integer>(); l1.add(4); l1.add(5); l1.add(6);
	 * System.out.println(getLowestPriority(l)); Decision d=new Decision();
	 * d.setAPSAndMedRecordsCandidateRequestList(l);
	 * d.setAPSAndMedRecordsFilteredRequestList(l1); filterAPSRequest(d);
	 * System.
	 * out.println(d.getAPSAndMedRecordsCandidateRequestList()+" candidate req "
	 * );
	 * System.out.println(d.getAPSAndMedRecordsFilteredRequestList()+" filtered req"
	 * ); }
	 */

	/*
	 * public static void main(String[] args) { List l=new ArrayList();
	 * l.add("A"); l.add("a"); List l1=new ArrayList(); //l1.add("");
	 * l1.add("a"); System.out.println(l.addAll(l1)); System.out.println(l); }
	 */
}
