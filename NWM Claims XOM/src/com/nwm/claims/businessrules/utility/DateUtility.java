package com.nwm.claims.businessrules.utility;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.Months;

import com.nwm.claims.businessrules.di.fasttrack.model.MedicalEvidence;
import com.nwm.claims.businessrules.di.fasttrack.model.Policy;

public class DateUtility {
	private static DateTime setDefaultTime(Date date) {
		if (date == null)
			return null;
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.HOUR, 0);
		DateTime dTime = new DateTime(cal.getTime());
		return dTime;
	}
	/*public static void main(String[] args) {
		String str="DI Claim Fast Track Eligibility.Fast Track Red Flag List.Medical Red Flag List.Disability Beyond Norm Red Flag Status.Calculate Normal Impairment Date.Setting Mean and Standard Dev with Primary.dta";
	String str1="DI Claim Fast Track Eligibility.Fast Track Red Flag List.Medical Red Flag List.Disability Beyond Norm Red Flag Status.Disability Beyond Norm Med RedFlag.Determine Disability Beyond Norm.brl";
	String str2="Determine Additional Medical Records List.Determine Priority For Impairments Specialties.Determine Prioritized Specialties Recommendation List.Setting MDSA General Specialty list into Prioritized Secondary Impairments Specialty List.brl";
	String str3="C:/Users/apuser4/Downloads/NWMClaims21062018/NWM Claims Rules/rules/Determine Additional Medical Record List/Determine Priority For Impairment Specialties/Determine Prioritized Specialty List/Determine Prioritized Specialty Recommendation List.brl";
	String str4="C:/Users/apuser4/Downloads/NWMClaims21062018 (1)/NWM Claims Rules/rules/DI Claim Fast Track Eligibility/Medical Red Flag List/Disability Beyond Norm Red Flag Status/Calculate Normal Impairment Date/Setting Mean and Standard Dev with Primary.dta";
	String str5="C:/Users/apuser4/Downloads/NWMClaims21062018 (1)/NWM Claims Rules/rules/DI Claim Fast Track Eligibility/Medical Red Flag List/Disability Beyond Norm Red Flag Status/Disability Beyond Norm Med RedFlag/Determine Disability Beyond Norm.brl";
	String str6="C:/Users/apuser4/Downloads/NWMClaims21062018 (1)/NWM Claims Rules/rules/Determine Additional Medical Record List/Determine Priority For Impairment Specialties/Determine Prioritized Specialty List/Setting MDSA General Specialty list for Secondary.brl";
	String str7="DI Claim Fast Track Eligibility.Fast Track Red Flag List.Medical Red Flag List.Disability Beyond Norm Red Flag Status.Calculate Normal Impairment Date";
	String str8="DI Claim Fast Track Eligibility.Fast Track Red Flag List.Medical Red Flag List.Disability Beyond Norm Red Flag Status.Disability Beyond Norm Med RedFlag";
	System.out.println(str.length()+" length "+str1.length());
	System.out.println(str2.length()+" length "+str3.length()+" four "+str4.length()+" five "+str5.length()+" six "+str6.length()+" seven "+str7.length()+" eight "+str8.length());
	}*/
	/*
	 * public static void main(String[] args) { Calendar
	 * c=Calendar.getInstance(); c.set(2017, 3,1); Date d=c.getTime();
	 * System.out.println(setDefaultDate(d)); }
	 */

	/*private static Date setDefaultDate(Date date) {
		if (date == null)
			return null;
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.HOUR, 0);
		return cal.getTime();

	}*/

	public static int durationInMonthsBetweenDates(Date issueDate, Date setupDate) {
if(issueDate!=null && setupDate!=null){
		Calendar start = Calendar.getInstance();
		start.setTime(issueDate);

		Calendar end = Calendar.getInstance();
		end.setTime(setupDate);

		int monthsBetween = 0;
		int dateDiff = end.get(Calendar.DAY_OF_MONTH) - start.get(Calendar.DAY_OF_MONTH);

		if (dateDiff > 0)
			monthsBetween++;

		monthsBetween += end.get(Calendar.MONTH) - start.get(Calendar.MONTH);
		monthsBetween += (end.get(Calendar.YEAR) - start.get(Calendar.YEAR)) * 12;
		return monthsBetween;
	}
return 0;
	}

	/*public static void main(String[] args) {
		Calendar calender = Calendar.getInstance();
		calender.set(2017, 9, 27);
		Calendar calender1 = Calendar.getInstance();
		calender1.set(2007, 01, 4);
		System.out.println(calender.getActualMaximum(Calendar.DAY_OF_MONTH));
		System.out.println(durationInMonthsBetweenDates(calender1.getTime(), calender.getTime()));
	}
*/
	public static Date getDateAfterYears(int year, Date aDate) {
		if (aDate == null)
			return null;
		DateTime dt = setDefaultTime(aDate);
		dt = dt.plusYears(year);
		return dt.toDate();
	}

	public static Date getDateBeforeYears(int year, Date aDate) {
		if (aDate == null)
			return null;
		DateTime dt = setDefaultTime(aDate);
		dt = dt.minusYears(year);
		return dt.toDate();
	}

	/*
	 * public static void main(String[] args) throws ParseException {
	 * System.out.println(getDateBeforeYears(0, new
	 * SimpleDateFormat("dd/MM/yyyy").parse("03/04/18"))); }
	 */

	// Check if date 1 is after date 2
	public static boolean checkDateAfter(Date date1, Date date2) {
		if (date1 == null || date2 == null)
			return false;

		DateTime dateTime1 = setDefaultTime(date1);
		DateTime dateTime2 = setDefaultTime(date2);
		return dateTime1.isAfter(dateTime2);

	}

	// Check if date 1 is before date 2
	public static boolean checkDateBefore(Date date1, Date date2) {

		if (date1 == null || date2 == null)
			return false;
		DateTime dateTime1 = setDefaultTime(date1);
		DateTime dateTime2 = setDefaultTime(date2);
		return dateTime1.isBefore(dateTime2);

	}

	public static int checkDurationInDays(Date date1, Date date2) {
		// Calendar cal = Calendar.getInstance();
		int days=0;
		if(date1 != null && date2 != null)
		{
	    days = Days.daysBetween(
	           new LocalDate(date1.getTime()), 
	           new LocalDate(date2.getTime())).getDays();
		}
		return days;
	}
	public static int checknDaysDifference(Date date1, Date date2) {
		// Calendar cal = Calendar.getInstance();
		int days=0;
		if(date1 != null && date2 != null)
		{
			if(date1.before(date2)){
	    days = Days.daysBetween(
	           new LocalDate(date1.getTime()), 
	           new LocalDate(date2.getTime())).getDays();
		}
			else{
				days = Days.daysBetween(
				           new LocalDate(date2.getTime()), 
				           new LocalDate(date1.getTime())).getDays();
					
			}
		}
	//System.out.println("Difference Days"+days);
		return days;
	}

	public static boolean checkOnorBefore(Date date1, Date date2) {
		return checkIfSameDate(date1, date2) || checkDateBefore(date1, date2);
	}

	public static boolean checkOnorAfter(Date date1, Date date2) {
		return checkIfSameDate(date1, date2) || checkDateAfter(date1, date2);
	}

	public static boolean checkIfSameDate(Date date1, Date date2) {
		if (date1 == null || date2 == null)
			return false;
		DateTime dateTime1 = setDefaultTime(date1);
		DateTime dateTime2 = setDefaultTime(date2);
		return dateTime1.isEqual(dateTime2);

	}

	/*public static Date getDateAfterGivenDays(int days, Date aDate) {
		if (aDate == null)
			return null;
		if (days == 0)
			return aDate;
		DateTime dt = setDefaultTime(aDate);
		dt = dt.plusDays(days);
		return dt.toDate();
	}*/

	public static Date getDateBeforeGivenDays(int days, Date aDate) {
		if (aDate == null)
			return null;
		if (days == 0)
			return aDate;
		DateTime dt = setDefaultTime(aDate);
		dt = dt.minusDays(days);
		return dt.toDate();
	}

	public static Date getDateBeforeGivenMonths(int months, Date aDate) {
		if (aDate == null)
			return null;
		if (months == 0)
			return aDate;
		DateTime dt = setDefaultTime(aDate);
		dt = dt.minusMonths(months);
		return dt.toDate();

	}

	public static Date getDateAfterGivenMonths(int months, Date aDate) {
		if (aDate == null)
			return null;
		if (months == 0)
			return aDate;
		DateTime dt = setDefaultTime(aDate);
		dt = dt.plusMonths(months);
		return dt.toDate();
	}

	public static Date getDateAfterGivenDays(int days, Date aDate) {
		if (aDate == null)
			return null;
		if (days == 0)
			return aDate;
		DateTime dt = setDefaultTime(aDate);
		/*
		 * String[] monthsDays=Double.toString(months).split("\\."); dt =
		 * dt.plusMonths(Integer.parseInt(monthsDays[0]));
		 * 
		 * dt = dt.plusDays(Integer.parseInt(monthsDays[1]));
		 */
		
		dt = dt.plusDays(days);
		return dt.toDate();
	}
/*public static void main(String[] args) {
	System.out.println(getDateAfterGivenDays(1307.26, new Date()));
}*/
	public static int checkDurationInMonths(Date onsetDate,Date setupDate) {
		// Calendar cal = Calendar.getInstance();
		// long duration = 0;
		long months = 0;
		if (onsetDate != null && setupDate!=null) {
			
			// System.out.println(dateTime2);
			months = Months.monthsBetween(setDefaultTime(onsetDate), setDefaultTime(setupDate)).getMonths();
			return (int) months;
		}
		return (int) months;			
	}
/*public static void main(String[] args) {
	Calendar c1=Calendar.getInstance();
	c1.set(2017, 01,01);
	Date d1=c1.getTime();
	c1.set(2018, 01,01);
	Date d2=c1.getTime();
	System.out.println(checkDurationInMonths(d1 ,d2));
}*/
	/*public static boolean checkIfDateIsBeforeCurrentDate(Date date) {
		if (date == null)
			return false;

		Date dateCurr = new Date();
		// System.out.println(dateCurr);

		return checkDateBefore(date, dateCurr);

	}
*/
	public static Date getCurrentDate() {
		Date now = null;

		try {
			Date date = new Date();
			String str = new SimpleDateFormat("dd/MM/yyyy").format(date);
			now = new SimpleDateFormat("dd/MM/yyyy").parse(str);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return now;
	}

	/*
	 * public static Date getEarliestDate(List<Date> dates) { Date earlierDate =
	 * null; if (dates != null && dates.size() > 0) { if (dates.size() == 1) {
	 * earlierDate = dates.get(0); } else if (dates.size() > 1) { earlierDate =
	 * Collections.min(dates);
	 * 
	 * } } return earlierDate; }
	 */
	public static Date getEarliestIssueDate(List<Policy> policies) {
		Date earliestIssueDate = null;
		List<Date> issueDates = null;
		if (policies != null && policies.size() > 0) {
			if (policies.size() == 1 && policies.get(0)!=null && policies.get(0).isDIPolicy()) {
				earliestIssueDate = policies.get(0).getIssueDate();
			} else if (policies.size() > 1) {
				issueDates = new ArrayList<Date>();
				for (Policy policy : policies) {
					if (policy!=null && policy.getIssueDate() != null && policy.isDIPolicy()) {
						issueDates.add(policy.getIssueDate());
					}
				}
				if (issueDates.size() > 0)
					earliestIssueDate = Collections.min(issueDates);
			}

		}
		return earliestIssueDate;
	}

	public static Date getLatestPolicyIssueDate(List<Policy> policies) {
		Date earliestIssueDate = null;
		List<Date> issueDates = null;
		if (policies != null && policies.size() > 0) {
			if (policies.size() == 1 && policies.get(0)!=null && policies.get(0).isDIPolicy()) {
				earliestIssueDate = policies.get(0).getIssueDate();

			} else if (policies.size() > 1) {
				issueDates = new ArrayList<Date>();
				for (Policy policy : policies) {
					if (policy!=null && policy.getIssueDate() != null && policy.isDIPolicy()) {
						issueDates.add(policy.getIssueDate());
					}
				}
				if (issueDates.size() > 0)
					earliestIssueDate = Collections.max(issueDates);
			}
			// System.out.println(earliestIssueDate+" issue date ");
		}

		return earliestIssueDate;
	}

	/*
	 * public static void main(String[] args) { Calendar
	 * c=Calendar.getInstance(); c.set(2017, 01, 01); Date d1=c.getTime();
	 * c.set(2019, 03, 01); Date d2=c.getTime(); List dates=new
	 * ArrayList<Policy>();
	 * 
	 * Policy p1=new Policy(); p1.setDIPolicy(true); p1.setIssueDate(d1); Policy
	 * p2=new Policy(); p2.setIssueDate(d2); p2.setDIPolicy(true);
	 * dates.add(p1); dates.add(p2);
	 * System.out.println(getLatestPolicyIssueDate(dates)); }
	 */
	public static Date getEarliestApplicationDate(List<Policy> policies) {
		Date earliestApplicationDate = null;
		List<Date> applicationDates = null;
		if (policies != null && policies.size() > 0) {
			if (policies.size() == 1 && policies.get(0)!=null && policies.get(0).isDIPolicy()) {
				earliestApplicationDate = policies.get(0).getApplicationDate();
			} else if (policies.size() > 1) {
				applicationDates = new ArrayList<Date>();
				for (Policy policy : policies) {
					if (policy!=null && policy.getApplicationDate() != null && policy.isDIPolicy()) {
						applicationDates.add(policy.getApplicationDate());
					}
				}
				if (applicationDates.size() > 0)
					earliestApplicationDate = Collections.min(applicationDates);
			}

		}
		return earliestApplicationDate;
	}

}

/*
 * public static void main(String[] args) throws ParseException { List dates=
 * new ArrayList<Date>(); dates.add(new
 * SimpleDateFormat("dd-MM-yyyy").parse("01-02-2018")); dates.add(new
 * SimpleDateFormat("dd-MM-yyyy").parse("02-02-2018")); dates.add(new
 * SimpleDateFormat("dd-MM-yyyy").parse("03-02-2018"));
 * 
 * Policy p1 = new Policy(); p1.setDIPolicy(false); p1.setIssueDate(null);
 * p1.setApplicationDate(null); Policy p2 = new Policy(); p2.setDIPolicy(true);
 * p2.setIssueDate(new SimpleDateFormat("dd-MM-yyyy").parse("04-02-2018"));
 * p2.setApplicationDate(new
 * SimpleDateFormat("dd-MM-yyyy").parse("07-02-2018")); Policy p3 = new
 * Policy(); p3.setDIPolicy(false); p3.setIssueDate(new
 * SimpleDateFormat("dd-MM-yyyy").parse("05-02-2018"));
 * p3.setApplicationDate(new
 * SimpleDateFormat("dd-MM-yyyy").parse("03-02-2018")); Policy p4 = new
 * Policy(); p4.setDIPolicy(true); p4.setIssueDate(new
 * SimpleDateFormat("dd-MM-yyyy").parse("07-02-2018"));
 * p4.setApplicationDate(new
 * SimpleDateFormat("dd-MM-yyyy").parse("03-02-2018")); List policies = new
 * ArrayList<Policy>(); policies.add(p1); policies.add(p2); policies.add(p3);
 * policies.add(p4);
 * 
 * System.out.println(getEarliestDate(dates));
 * System.out.println(getEarliestIssueDate(policies));
 * System.out.println(getEarliestApplicationDate(policies)); }
 */

/*
 * public static void main(String[] args) {
 * System.out.println(getCurrentDate()); }
 */
// public static void main(String[] args) {
// // SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
// // try {
// // Date birthDate = formatter.parse("13-02-1991");
// // System.out.println(getDateBeforeYears(birthDate, 3));
// //
// // } catch (Exception ex) {
// // ex.printStackTrace();
// // }
// // System.out.println(getDateBeforeYears(new Date(), 3));
//
// Calendar cd = Calendar.getInstance();
// cd.set(2000, 04, 15);
//
// Date dt1 = cd.getTime();
// cd.set(2000, 04, 14);
// Date dt2 = cd.getTime();
//
// System.out.println(DateUtility.checkDateAfter(dt1, dt2));
//
// }

// public static void main(String[] args) {
// Calendar cd = Calendar.getInstance();
// cd.set(2014, 8, 4);
// Date dt1 = cd.getTime();
// cd.set(2018, 8, 4);
// Date dt2 = cd.getTime();
// System.out.println( checkIfDateIsBeforeCurrentDate(dt2));

// System.out.println(checkDuration(dt1, dt2));
// System.out.println("Is After"+checkDateAfter(dt1, dt2));
// System.out.println("Is Before"+checkDateBefore(dt1, dt2));
// System.out.println("Is Equal"+checkIfSameDate(dt1, dt2));
// System.out.println("Is on or after"+checkOnorAfter(dt1, dt2));
// System.out.println("Is on or before"+checkOnorBefore(dt1, dt2));

// System.out.println(s.charAt(0));
//
// }
