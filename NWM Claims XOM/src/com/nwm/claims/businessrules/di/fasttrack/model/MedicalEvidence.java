package com.nwm.claims.businessrules.di.fasttrack.model;

import ilog.rules.bom.annotations.BusinessType;

import java.io.Serializable;
import java.util.Date;

public class MedicalEvidence implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2598435373517206856L;
	private Date fromDate;
	private Date toDate;
	private String type;
	private String medicalEvidenceID = "";
	private String providerSpecialty="";
	private String treatmentDateApplicability="";

	@BusinessType("VirtualDomains.MedicalEvidenceSpecialty")
	public String getProviderSpecialty() {
		return providerSpecialty;
	}

	public void setProviderSpecialty(String providerSpecialty) {
		this.providerSpecialty = providerSpecialty;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	
	
	
	public String getMedicalEvidenceID() {
		return medicalEvidenceID;
	}

	public void setMedicalEvidenceID(String medicalEvidenceID) {
		this.medicalEvidenceID = medicalEvidenceID;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	@BusinessType("VirtualDomains.TreatmentDatesApplicable")
	public String getTreatmentDateApplicability() {
		return treatmentDateApplicability;
	}

	public void setTreatmentDateApplicability(String treatmentDateApplicability) {
		this.treatmentDateApplicability = treatmentDateApplicability;
	}
	
}
