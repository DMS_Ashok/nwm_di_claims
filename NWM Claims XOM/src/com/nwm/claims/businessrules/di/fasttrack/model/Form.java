package com.nwm.claims.businessrules.di.fasttrack.model;

import ilog.rules.bom.annotations.BusinessType;

import java.io.Serializable;
import java.util.Date;

public class Form implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1077976641150433654L;
	private String altered  ;
	private String received ;
	private String signed;
	private String type;
	private Date dob;
	// Removed as on 03/10/2018 - Arnab
	// private String docType = "";
	private String ssn;

	@BusinessType("VirtualDomains.Altered")
	public String getAltered() {
		if(altered==null)
			altered="";
		return altered;
	}

	public void setAltered(String altered) {
		this.altered = altered;
	}

	@BusinessType("VirtualDomains.Received")
	public String getReceived() {
		if(received==null)
		received="";
		return received;
	}

	public void setReceived(String received) {
		this.received = received;
	}

	@BusinessType("VirtualDomains.Signed")
	public String getSigned() {
		if(signed==null)
			signed="";
		return signed;
	}

	public void setSigned(String signed) {
		this.signed = signed;
	}

	public @BusinessType("VirtualDomains.FormType") String getType() {
		if(type==null)
			type="";
		return type;
	}

	public void setType(@BusinessType("VirtualDomains.FormType") String type) {
		this.type = type;
	}

	public String getSsn() {
		if(ssn==null)
			ssn="";
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}
}
