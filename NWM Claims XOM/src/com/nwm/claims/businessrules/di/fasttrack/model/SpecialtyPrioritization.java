package com.nwm.claims.businessrules.di.fasttrack.model;

import java.io.Serializable;
import java.util.Comparator;

public class SpecialtyPrioritization implements Serializable{
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private String impairmentCode;
private String EvidenceSpecialty;
private int priority;
public String getImpairmentCode() {
	return impairmentCode;
}
public void setImpairmentCode(String impairmentCode) {
	this.impairmentCode = impairmentCode;
}
public String getEvidenceSpecialty() {
	return EvidenceSpecialty;
}
public void setEvidenceSpecialty(String evidenceSpecialty) {
	EvidenceSpecialty = evidenceSpecialty;
}
public int getPriority() {
	return priority;
}
public void setPriority(int priority) {
	this.priority = priority;
}

 public static Comparator<SpecialtyPrioritization> priorityComparator = new Comparator<SpecialtyPrioritization>() {
	public int compare(SpecialtyPrioritization o1, SpecialtyPrioritization o2) {
	// TODO Auto-generated method stub
	if(o1!=null && o2!=null){
		if(o1.getPriority()>o2.getPriority()){
			return 1;
		}
		else if(o1.getPriority()<o2.getPriority()){
			return -1;
		}
	}
	return 0;
}
 };

 public boolean equals(Object obj)
 {
	 try
	 {
		 SpecialtyPrioritization sp=(SpecialtyPrioritization)obj;
		 if(this.priority== sp.priority && this.EvidenceSpecialty.equals(sp.EvidenceSpecialty) && this.impairmentCode.equals(sp.impairmentCode))
		 	return true;
		 else
			 return false;
	 }
	 catch(ClassCastException  | NullPointerException e)
	 {
		 return false;
	 }
	
 }


}
