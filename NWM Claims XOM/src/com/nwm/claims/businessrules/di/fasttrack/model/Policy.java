package com.nwm.claims.businessrules.di.fasttrack.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Policy implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8758949097069793780L;
	private Date issueDate;
	private String policyNumber = "";
	private String planCode = "";
	private Date applicationDate;
	private boolean isDIPolicy;

	// private double sumInsured = 0.0;
	// private String policyType = "";
	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	// public double getSumInsured() {
	// return sumInsured;
	// }
	//
	// public void setSumInsured(double sumInsured) {
	// this.sumInsured = sumInsured;
	// }

	/*
	 * public String getPolicyType() { return policyType; }
	 * 
	 * public void setPolicyType(String policyType) { this.policyType =
	 * policyType; }
	 */
	public String getPlanCode() {
		return planCode;
	}

	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}

	public Date getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	public boolean isDIPolicy() {
		return isDIPolicy;
	}

	public void setDIPolicy(boolean isDIPolicy) {
		this.isDIPolicy = isDIPolicy;
	}


}
