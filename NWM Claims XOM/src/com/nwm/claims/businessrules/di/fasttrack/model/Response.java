package com.nwm.claims.businessrules.di.fasttrack.model;

import ilog.rules.bom.annotations.BusinessType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Response implements Serializable {
	private static final long serialVersionUID = -8084470256490548760L;
	private List<Message> ruleMessages;

	protected List<Message> addMessageToMessages(Message message,
			List<Message> messages) {
		if (null == messages)
			messages = new ArrayList<Message>();
		messages.add(message);
		return messages;

	}

	//
	// public void addRuleMessages(String code, String type, String severity,
	// String description, String dest) {
	// }

	public void addRuleMessages(@BusinessType("VirtualDomains.MessageCode")String code,@BusinessType("VirtualDomains.MessageType") String type,@BusinessType("VirtualDomains.MessageSeverity") String severity,
			String description, String dest) {
		Message message = null;
		if (dest.equals("RULE")) {
			message = new Message(code, type, severity, description);
			this.addMessageToMessages(message, this.getRuleMessages());
		}

	}
	public void addRuleMessages(String impairmentName, String category,String description, String dest) {
		Message message = null;
		if (dest.equals("RULE")) {
			message = new Message(impairmentName, category,description);
			this.addMessageToMessages(message, this.getRuleMessages());
		}

	}

	public List<Message> getRuleMessages() {
		if (ruleMessages == null)
			ruleMessages = new ArrayList<Message>();
		return ruleMessages;
	}

	public void setRuleMessages(List<Message> ruleMessages) {
		this.ruleMessages = ruleMessages;
	}

	// public List<Message> getApplicationMessages() {
	// return applicationMessages;
	// }
	//
	// public void setApplicationMessages(List<Message> applicationMessages) {
	// this.applicationMessages = applicationMessages;
	// }
	// public static void main(String[] args) {
	// Response response = new Response();
	// response.addRuleMessages("1000", "App", "Info", "New Message", "RULE");
	// System.out.println(response.getRuleMessages().size());
	// }

}
