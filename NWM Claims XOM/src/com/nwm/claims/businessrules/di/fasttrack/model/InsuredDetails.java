package com.nwm.claims.businessrules.di.fasttrack.model;

import ilog.rules.bom.annotations.BusinessType;

import java.io.Serializable;
import java.util.Date;

public class InsuredDetails implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5808660809670036061L;
	// private String taxPayerID;
	private String ssn = "";
	private String hasPriorClaim;
	private int indexBureauAccidentCount = 0;
	private Date priorClaimPaidToDate;
	private Date dob;

	// private List<Address> address = new ArrayList<Address>();

	// public List<Address> getAddress() {
	// return address;
	// }
	//
	// public void setAddress(List<Address> address) {
	// this.address = address;
	// }
	@BusinessType("VirtualDomains.InsuredHasPriorClaim")
	public String getHasPriorClaim() {
		if(hasPriorClaim==null)
			hasPriorClaim="";
		return hasPriorClaim;
	}

	public void setHasPriorClaim(String hasPriorClaim) {
		this.hasPriorClaim = hasPriorClaim;
	}

	public int getIndexBureauAccidentCount() {
		return indexBureauAccidentCount;
	}

	public void setIndexBureauAccidentCount(int indexBureauAccidentCount) {
		this.indexBureauAccidentCount = indexBureauAccidentCount;
	}

	public Date getPriorClaimPaidToDate() {
		return priorClaimPaidToDate;
	}

	public void setPriorClaimPaidToDate(Date priorClaimPaidToDate) {
		this.priorClaimPaidToDate = priorClaimPaidToDate;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

}
