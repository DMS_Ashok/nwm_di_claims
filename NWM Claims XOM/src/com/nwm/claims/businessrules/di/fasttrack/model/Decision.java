package com.nwm.claims.businessrules.di.fasttrack.model;

import ilog.rules.bom.annotations.BusinessType;
import ilog.rules.bom.annotations.CollectionDomain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class Decision extends Response implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4213077838611281539L;
	
	private String claimNumber;
	private String hasDIPolicyForFastTrack;
	private List<String> businessPersonalRedFlagList;
	private List<String> claimDocumentationRedFlagList;
	private String claimRecoveryStatus;
	private List<String> medicalRedFlagList;
	private List<String> fastTrackRedFlagList;
	private List<String> secondarySFMCList;
	private String allImpairmentsAreInSFMC;
	private String authorizationFormIGOStatus;
	private String requestForDisabilityFormIGOStatus;
	private String fastTrackRequiredDocumentsIGOStatus;
	private int durationInDaysBetweenOnsetAndSetupDate;
	private int durationInDaysBetweenOnsetAndRecoveryDate;
	private String fastTrackEligibility;
	private Date treatmentBeginDate;
	private Date treatmentEndDate;
	private double mean;
	private double standardDeviation;
	private Date normalImpairmentDate;
	private List<String> primaryImpairmentSpecialtyList;
	private List<String> secondaryImpairmentSpecialtyList;
	private List<String> applicableEvidenceSpecialtyList;
	private String hasDIPolicyForAI;
	private List<String> AIRedFlagList;
	private List<SpecialtyPrioritization> prioritizedPrimaryMedicalRecordsList;
	private List<SpecialtyPrioritization> prioritizedSecondaryMedicalRecordsList;
	
	/*private List<String> filteredSpecialtiesRecommendationListForPrimaryImpairment;
	private List<String> filteredSpecialtiesRecommendationListForSecondaryImpairment;*/
	
	private List<String> prioritizedFilteredSpecialtiesForPrimaryImpairment;
	private List<String> prioritizedFilteredSpecialtiesForSecondaryImpairments;

	@CollectionDomain(elementType = "VirtualDomains.MedicalEvidenceSpecialty")
	public List<String> getApplicableEvidenceSpecialtyList() {
		if (applicableEvidenceSpecialtyList == null)
			applicableEvidenceSpecialtyList = new ArrayList<String>();
		return applicableEvidenceSpecialtyList;
	}

	public void setApplicableEvidenceSpecialtyList(List<String> applicableEvidenceSpecialtyList) {
		this.applicableEvidenceSpecialtyList = applicableEvidenceSpecialtyList;
	}

	/*@CollectionDomain(elementType = "VirtualDomains.MedicalEvidenceSpecialty")
	public List<String> getFilteredSpecialtiesRecommendationListForPrimaryImpairment() {
		if (filteredSpecialtiesRecommendationListForPrimaryImpairment == null)
			filteredSpecialtiesRecommendationListForPrimaryImpairment = new ArrayList<String>();
		return filteredSpecialtiesRecommendationListForPrimaryImpairment;
	}

	public void setFilteredSpecialtiesRecommendationListForPrimaryImpairment(
			List<String> filteredSpecialtiesRecommendationListForPrimaryImpairment) {
		this.filteredSpecialtiesRecommendationListForPrimaryImpairment = filteredSpecialtiesRecommendationListForPrimaryImpairment;
	}

	public List<String> getFilteredSpecialtiesRecommendationListForSecondaryImpairment() {
		if (filteredSpecialtiesRecommendationListForSecondaryImpairment == null)
			filteredSpecialtiesRecommendationListForSecondaryImpairment = new ArrayList<String>();
		return filteredSpecialtiesRecommendationListForSecondaryImpairment;
	}

	public void setFilteredSpecialtiesRecommendationListForSecondaryImpairment(
			List<String> filteredSpecialtiesRecommendationListForSecondaryImpairment) {
		this.filteredSpecialtiesRecommendationListForSecondaryImpairment = filteredSpecialtiesRecommendationListForSecondaryImpairment;
	}
*/
	@CollectionDomain(elementType = "VirtualDomains.MedicalEvidenceSpecialty")
	public List<String> getPrioritizedFilteredSpecialtiesForPrimaryImpairment() {
		if(prioritizedFilteredSpecialtiesForPrimaryImpairment==null)
			prioritizedFilteredSpecialtiesForPrimaryImpairment=new ArrayList<String>();
		return prioritizedFilteredSpecialtiesForPrimaryImpairment;
	}

	public void setPrioritizedFilteredSpecialtiesForPrimaryImpairment(
			List<String> prioritizedFilteredSpecialtiesForPrimaryImpairment) {
		this.prioritizedFilteredSpecialtiesForPrimaryImpairment = prioritizedFilteredSpecialtiesForPrimaryImpairment;
	}

	@CollectionDomain(elementType = "VirtualDomains.MedicalEvidenceSpecialty")
	public List<String> getPrioritizedFilteredSpecialtiesForSecondaryImpairments() {
		if(prioritizedFilteredSpecialtiesForSecondaryImpairments==null)
			prioritizedFilteredSpecialtiesForSecondaryImpairments=new ArrayList<String>();
		return prioritizedFilteredSpecialtiesForSecondaryImpairments;
	}

	public void setPrioritizedFilteredSpecialtiesForSecondaryImpairments(
			List<String> prioritizedFilteredSpecialtiesForSecondaryImpairments) {
		this.prioritizedFilteredSpecialtiesForSecondaryImpairments = prioritizedFilteredSpecialtiesForSecondaryImpairments;
	}
	
	
	

	// private Date policyIssueDate;
	// private Date policyApplicationDate;
	/*@BusinessType("VirtualDomains.ImpairmentIsMDSACondition")
	public String getPrimaryImpairmentIsMDSA() {
		if(primaryImpairmentIsMDSA==null)
			primaryImpairmentIsMDSA="";
		return primaryImpairmentIsMDSA;
	}

	public void setPrimaryImpairmentIsMDSA(String primaryImpairmentIsMDSA) {
		this.primaryImpairmentIsMDSA = primaryImpairmentIsMDSA;
	}*/

	public double getMean() {
		return mean;
	}

	public void setMean(double mean) {
		this.mean = mean;
	}

	public double getStandardDeviation() {
		return standardDeviation;
	}

	public void setStandardDeviation(double standardDeviation) {
		this.standardDeviation = standardDeviation;
	}

	@BusinessType("VirtualDomains.HasDIPolicy")
	public String getHasDIPolicyForFastTrack() {
		if(hasDIPolicyForFastTrack==null)
			hasDIPolicyForFastTrack="";
		return hasDIPolicyForFastTrack;
	}

	public void setHasDIPolicyForFastTrack(String hasDIPolicyForFastTrack) {
		this.hasDIPolicyForFastTrack = hasDIPolicyForFastTrack;
	}

	// private List<Message> ruleMessages=new ArrayList<Message>();

	@CollectionDomain(elementType = "VirtualDomains.BusinessPersonalRedFlag")
	public List<String> getBusinessPersonalRedFlagList() {
		if (businessPersonalRedFlagList == null)
			businessPersonalRedFlagList = new ArrayList<String>();
		return businessPersonalRedFlagList;
	}
	@BusinessType("VirtualDomains.FastTrakReqDocInGoodOrder")
	public String getAuthorizationFormIGOStatus() {
		if(authorizationFormIGOStatus==null)
			authorizationFormIGOStatus="";
		return authorizationFormIGOStatus;
	}
	public void setAuthorizationFormIGOStatus(String authorizationFormIGOStatus) {
		this.authorizationFormIGOStatus = authorizationFormIGOStatus;
	}
	@BusinessType("VirtualDomains.FastTrakReqDocInGoodOrder")
	public String getRequestForDisabilityFormIGOStatus() {
		if(requestForDisabilityFormIGOStatus==null)
			requestForDisabilityFormIGOStatus="";
		return requestForDisabilityFormIGOStatus;
	}

	public void setRequestForDisabilityFormIGOStatus(
			String requestForDisabilityFormIGOStatus) {
		this.requestForDisabilityFormIGOStatus = requestForDisabilityFormIGOStatus;
	}

	public void setBusinessPersonalRedFlagList(
			@CollectionDomain(elementType = "VirtualDomains.BusinessPersonalRedFlag") List<String> businessPersonalRedFlagList) {
		this.businessPersonalRedFlagList = businessPersonalRedFlagList;
	}

	@CollectionDomain(elementType = "VirtualDomains.ClaimDocRedFlags")
	public List<String> getClaimDocumentationRedFlagList() {
		if (claimDocumentationRedFlagList == null)
			claimDocumentationRedFlagList = new ArrayList<String>();
		return claimDocumentationRedFlagList;
	}

	public void setClaimDocumentationRedFlagList(
			@CollectionDomain(elementType = "VirtualDomains.ClaimDocRedFlags") List<String> claimDocumentationRedFlagList) {
		this.claimDocumentationRedFlagList = claimDocumentationRedFlagList;
	}

	@BusinessType("VirtualDomains.ClaimRecoveryStatus")
	public String getClaimRecoveryStatus() {
		return claimRecoveryStatus;
	}

	public void setClaimRecoveryStatus(@BusinessType("VirtualDomains.ClaimRecoveryStatus") String claimRecoveryStatus) {
		this.claimRecoveryStatus = claimRecoveryStatus;
	}

	@CollectionDomain(elementType = "VirtualDomains.FastTrackRedFlags")
	public List<String> getFastTrackRedFlagList() {
		if (fastTrackRedFlagList == null)
			fastTrackRedFlagList = new ArrayList<String>();
		return fastTrackRedFlagList;
	}

	public void setFastTrackRedFlagList(
			@CollectionDomain(elementType = "VirtualDomains.FastTrackRedFlags") List<String> fastTrackRedFlagList) {
		this.fastTrackRedFlagList = fastTrackRedFlagList;
	}

	@CollectionDomain(elementType = "VirtualDomains.MedicalRedFlags")
	public List<String> getMedicalRedFlagList() {
		if (medicalRedFlagList == null)
			medicalRedFlagList = new ArrayList<String>();
		return medicalRedFlagList;
	}

	public void setMedicalRedFlagList(@CollectionDomain(elementType = "VirtualDomains.MedicalRedFlags") List<String> medicalRedFlagList) {
		this.medicalRedFlagList = medicalRedFlagList;
	}

	
	public Date getTreatmentEndDate() {
		return treatmentEndDate;
	}

	public void setTreatmentEndDate(Date treatmentEndDate) {
		this.treatmentEndDate = treatmentEndDate;
	}

	public Date getTreatmentBeginDate() {
		return treatmentBeginDate;
	}

	public void setTreatmentBeginDate(Date treatmentBeginDate) {
		this.treatmentBeginDate = treatmentBeginDate;
	}

	@BusinessType("VirtualDomains.DIClaimFastTrackEligibility")
	public String getFastTrackEligibility() {
		return fastTrackEligibility;
	}

	public void setFastTrackEligibility(@BusinessType("VirtualDomains.DIClaimFastTrackEligibility") String fastTrackEligibility) {
		this.fastTrackEligibility = fastTrackEligibility;
	}

	// public List<Message> getRuleMessages() {
	// if (ruleMessages == null)
	// ruleMessages = new ArrayList<Message>();
	// return ruleMessages;
	// }
	//
	// public void setRuleMessages(List<Message> ruleMessages) {
	// this.ruleMessages = ruleMessages;
	// }
	@BusinessType("VirtualDomains.FastTrakReqDocInGoodOrder")
	public String getFastTrackRequiredDocumentsIGOStatus() {
		return fastTrackRequiredDocumentsIGOStatus;
	}

	public void setFastTrackRequiredDocumentsIGOStatus(
			@BusinessType("VirtualDomains.FastTrakReqDocInGoodOrder") String fastTrackRequiredDocumentsIGOStatus) {
		this.fastTrackRequiredDocumentsIGOStatus = fastTrackRequiredDocumentsIGOStatus;
	}

	public String getClaimNumber() {
		return claimNumber;
	}

	public void setClaimNumber(String claimNumber) {
		this.claimNumber = claimNumber;
	}
	
	@BusinessType("VirtualDomains.HasDIPolicy")
	public String getHasDIPolicyForAI() {
		if(hasDIPolicyForAI==null)
			hasDIPolicyForAI="";
		return hasDIPolicyForAI;
	}

	public void setHasDIPolicyForAI(String hasDIPolicyForAI) {
		this.hasDIPolicyForAI = hasDIPolicyForAI;
	}
	@CollectionDomain(elementType = "VirtualDomains.ClaimDocRedFlags")
	public List<String> getAIRedFlagList() {
		if(AIRedFlagList==null)
			AIRedFlagList=new ArrayList<String>();
		return AIRedFlagList;
	}

	public void setAIRedFlagList(List<String> aIRedFlagList) {
		AIRedFlagList = aIRedFlagList;
	}
/*
	@CollectionDomain(elementType = "VirtualDomains.MedicalEvidenceSpecialty")
	public List<String> getPrimaryMedicalEvidenceSpecialtyApplicableList() {
		if (primaryMedicalEvidenceSpecialtyApplicableList == null)
			primaryMedicalEvidenceSpecialtyApplicableList = new ArrayList<String>();
		return primaryMedicalEvidenceSpecialtyApplicableList;
	}

	public void setPrimaryMedicalEvidenceSpecialtyApplicableList(List<String> primaryMedicalEvidenceSpecialtyApplicableList) {
		this.primaryMedicalEvidenceSpecialtyApplicableList = primaryMedicalEvidenceSpecialtyApplicableList;
	}

	@CollectionDomain(elementType = "VirtualDomains.MedicalEvidenceSpecialty")
	public List<String> getSecondaryMedicalEvidenceSpecialtyApplicableList() {
		if (secondaryMedicalEvidenceSpecialtyApplicableList == null)
			secondaryMedicalEvidenceSpecialtyApplicableList = new ArrayList<String>();
		return secondaryMedicalEvidenceSpecialtyApplicableList;
	}

	public void setSecondaryMedicalEvidenceSpecialtyApplicableList(List<String> secondaryMedicalEvidenceSpecialtyApplicableList) {
		this.secondaryMedicalEvidenceSpecialtyApplicableList = secondaryMedicalEvidenceSpecialtyApplicableList;
	}
*/
	@CollectionDomain(elementType = "VirtualDomains.MedicalEvidenceSpecialty")
	public List<String> getSecondaryImpairmentSpecialtyList() {
		if (secondaryImpairmentSpecialtyList == null)
			secondaryImpairmentSpecialtyList = new ArrayList<String>();
		return secondaryImpairmentSpecialtyList;
	}

	public void setSecondaryImpairmentSpecialtyList(List<String> secondaryImpairmentSpecialtyList) {
		this.secondaryImpairmentSpecialtyList = secondaryImpairmentSpecialtyList;
	}

	@CollectionDomain(elementType = "VirtualDomains.MedicalEvidenceSpecialty")
	public List<String> getPrimaryImpairmentSpecialtyList() {
		if (primaryImpairmentSpecialtyList == null)
			primaryImpairmentSpecialtyList = new ArrayList<String>();
		return primaryImpairmentSpecialtyList;
	}

	public void setPrimaryImpairmentSpecialtyList(List<String> primaryImpairmentSpecialtyList) {
		this.primaryImpairmentSpecialtyList = primaryImpairmentSpecialtyList;
	}

	/*public List<String> getSecondaryMDSAImpairmentCodes() {
		if (secondaryMDSAImpairmentCodes == null)
			secondaryMDSAImpairmentCodes = new ArrayList<String>();
		return secondaryMDSAImpairmentCodes;
	}

	public void setSecondaryMDSAImpairmentCodes(List<String> secondaryMDSAImpairmentCodes) {
		this.secondaryMDSAImpairmentCodes = secondaryMDSAImpairmentCodes;
	}*/

	public List<SpecialtyPrioritization> getPrioritizedPrimaryMedicalRecordsList() {
		if (prioritizedPrimaryMedicalRecordsList == null)
			prioritizedPrimaryMedicalRecordsList = new ArrayList<SpecialtyPrioritization>();
		return prioritizedPrimaryMedicalRecordsList;
	}

	public void setPrioritizedPrimaryMedicalRecordsList(List<SpecialtyPrioritization> prioritizedPrimaryMedicalRecordsList) {
		this.prioritizedPrimaryMedicalRecordsList = prioritizedPrimaryMedicalRecordsList;
	}

	public List<SpecialtyPrioritization> getPrioritizedSecondaryMedicalRecordsList() {
		if (prioritizedSecondaryMedicalRecordsList == null)
			prioritizedSecondaryMedicalRecordsList = new ArrayList<SpecialtyPrioritization>();
		return prioritizedSecondaryMedicalRecordsList;
	}

	public void setPrioritizedSecondaryMedicalRecordsList(List<SpecialtyPrioritization> prioritizedSecondaryMedicalRecordsList) {
		this.prioritizedSecondaryMedicalRecordsList = prioritizedSecondaryMedicalRecordsList;
	}

	public Date getNormalImpairmentDate() {
		return normalImpairmentDate;
	}

	public void setNormalImpairmentDate(Date normalImpairmentDate) {
		this.normalImpairmentDate = normalImpairmentDate;
	}

	public List<String> getSecondarySFMCList() {
		if(secondarySFMCList==null)
			secondarySFMCList=new ArrayList<String>();
		return secondarySFMCList;
	}

	public void setSecondarySFMCList(List<String> secondarySFMCList) {
		this.secondarySFMCList = secondarySFMCList;
	}

	public String getAllImpairmentsAreInSFMC() {
		
		return allImpairmentsAreInSFMC;
	}

	public void setAllImpairmentsAreInSFMC(String allImpairmentsAreInSFMC) {
		this.allImpairmentsAreInSFMC = allImpairmentsAreInSFMC;
	}

	public int getDurationInDaysBetweenOnsetAndSetupDate() {
		
		return durationInDaysBetweenOnsetAndSetupDate;
	}

	public void setDurationInDaysBetweenOnsetAndSetupDate(
			int durationInDaysBetweenOnsetAndSetupDate) {
		this.durationInDaysBetweenOnsetAndSetupDate = durationInDaysBetweenOnsetAndSetupDate;
	}

	public int getDurationInDaysBetweenOnsetAndRecoveryDate() {
		
		return durationInDaysBetweenOnsetAndRecoveryDate;
	}

	public void setDurationInDaysBetweenOnsetAndRecoveryDate(
			int durationInDaysBetweenOnsetAndRecoveryDate) {
		this.durationInDaysBetweenOnsetAndRecoveryDate = durationInDaysBetweenOnsetAndRecoveryDate;
	}

	

	
	
	

	

	// private String dIPolicyCheck;
	//
	// public String getdIPolicyCheck() {
	// return dIPolicyCheck;
	// }
	//
	// public void setdIPolicyCheck(String dIPolicyCheck) {
	// this.dIPolicyCheck = dIPolicyCheck;
	// }

}
