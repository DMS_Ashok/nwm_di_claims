package com.nwm.claims.businessrules.di.fasttrack.model;

import java.io.Serializable;

public class Message implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1062091502320922263L;
	private String code="";
	private String type="";
	private String severity="";
	private String description="";
	private String impairmentName="";
	private String impairmentCategory="";

	public Message(String code, String type, String severity, String description) {
		this.code = code;
		this.type = type;
		this.severity = severity;
		this.description = description;
	}
	public Message(String description, String name, String category) {
		this.description=description;
		this.setImpairmentCategory(category);
		this.impairmentName = name;
		
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImpairmentName() {
		return impairmentName;
	}

	public void setImpairmentName(String impairmentName) {
		this.impairmentName = impairmentName;
	}

	public static Message createMessage(String code, String type,
			String severity, String description) {
		Message message = new Message(code, type, severity, description);
		return message;

	}
	public String getImpairmentCategory() {
		return impairmentCategory;
	}
	public void setImpairmentCategory(String impairmentCategory) {
		this.impairmentCategory = impairmentCategory;
	}

}
