package com.nwm.claims.businessrules.di.fasttrack.model;

import ilog.rules.bom.annotations.BusinessType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Claim implements Serializable {
	private static final long serialVersionUID = -1356878699583736552L;
	private String claimNumber = "";
	private List<Form> form = new ArrayList<Form>();
	private List<Policy> policies = new ArrayList<Policy>();
	private String primaryMedicalImpairmentCode = "";
	private List<SecondaryImpairment> secondaryImpairments;
	private int businessSFMCScoreForPrimary;
	
	private int analyticSFMCScoreForPrimary;
	
	private List<MedicalEvidence> medicalEvidenceList;
	private Date recoveryDate;
	private Date setupDate;
	private Date onsetDate;
	private Date accidentDate;
	private String insuredMissingHomeAddress;
	private InsuredDetails enterpriseInsuredData;
	private InsuredDetails claimInsuredDetails;
    private String accidentIllnessFlag;
    private String medicalExamRequestStatus;
    private String medicalReferralPainMedicationEvaluation;       
   
  /*  public String getMedicalReferralPainMedicationEvaluation() {
		return medicalReferralPainMedicationEvaluation;
	}*/
    @BusinessType("VirtualDomains.PainMedicationEvaluation")
    public String getMedicalReferralPainMedicationEvaluation() {
    	if(medicalReferralPainMedicationEvaluation==null)
    		medicalReferralPainMedicationEvaluation="";
		return medicalReferralPainMedicationEvaluation;
	}
    
	public void setMedicalReferralPainMedicationEvaluation(
			String medicalReferralPainMedicationEvaluation) {
		this.medicalReferralPainMedicationEvaluation = medicalReferralPainMedicationEvaluation;
	}	
    
	@BusinessType("VirtualDomains.AccidentIllnessFlag")
	public String getAccidentIllnessFlag() {
		if(accidentIllnessFlag==null)
			accidentIllnessFlag="";
		return accidentIllnessFlag;
	}

	public void setAccidentIllnessFlag(String accidentIllnessFlag) {
		this.accidentIllnessFlag = accidentIllnessFlag;
	}
	@BusinessType("VirtualDomains.MedicalExamRequestStatus")
	public String getMedicalExamRequestStatus() {
		if(medicalExamRequestStatus==null)
			medicalExamRequestStatus="";
		return medicalExamRequestStatus;
	}

	public void setMedicalExamRequestStatus(String medicalExamRequestStatus) {
		this.medicalExamRequestStatus = medicalExamRequestStatus;
	}

	public InsuredDetails getEnterpriseInsuredData() {
		return enterpriseInsuredData;
	}

	public void setEnterpriseInsuredData(InsuredDetails enterpriseInsuredData) {
		this.enterpriseInsuredData = enterpriseInsuredData;
	}

	public List<Policy> getPolicies() {
		if(policies==null)
			policies=new ArrayList<Policy>();
		return policies;
	}

	public void setPolicies(List<Policy> policies) {
		this.policies = policies;
	}

	public Date getOnsetDate() {
		return onsetDate;
	}

	public void setOnsetDate(Date onsetDate) {
		this.onsetDate = onsetDate;
	}

	public Date getRecoveryDate() {
		//System.out.println("recoveryDate"+recoveryDate);
		return recoveryDate;
	}

	public void setRecoveryDate(Date recoveryDate) {
		this.recoveryDate = recoveryDate;
	}

	public String getClaimNumber() {
		return claimNumber;
	}

	public void setClaimNumber(String claimNumber) {
		this.claimNumber = claimNumber;
	}

	public String getPrimaryMedicalImpairmentCode() {
		if(primaryMedicalImpairmentCode==null)
			primaryMedicalImpairmentCode="";
		return primaryMedicalImpairmentCode;
	}

	public void setPrimaryMedicalImpairmentCode(
			String primaryMedicalImpairmentCode) {
		this.primaryMedicalImpairmentCode = primaryMedicalImpairmentCode;
	}

	public List<Form> getForm() {
		if(form==null)
			form=new ArrayList<Form>();
		return form;
	}

	public void setForm(List<Form> form) {
		this.form = form;
	}

	/*public List<Policy> getPolicyList() {
		return policyList;
	}

	public void setPolicyList(List<Policy> policyList) {
		this.policyList = policyList;
	}*/
	public List<MedicalEvidence> getMedicalEvidenceList() {
		if (medicalEvidenceList == null)
			medicalEvidenceList = new ArrayList<MedicalEvidence>();
		return medicalEvidenceList;
	}

	
	public void setMedicalEvidenceList(List<MedicalEvidence> medicalEvidenceList) {
		this.medicalEvidenceList = medicalEvidenceList;
	}
	public Date getSetupDate() {
		return setupDate;
	}

	public void setSetupDate(Date setupDate) {
		this.setupDate = setupDate;
	}

	public Date getAccidentDate() {
		return accidentDate;
	}

	public void setAccidentDate(Date accidentDate) {
		this.accidentDate = accidentDate;
	}
	@BusinessType("VirtualDomains.InsuredMissingHomeAddress")
	public String getInsuredMissingHomeAddress() {
		if(insuredMissingHomeAddress==null)
			insuredMissingHomeAddress="";
		return insuredMissingHomeAddress;
	}

	public void setInsuredMissingHomeAddress(String insuredMissingHomeAddress) {
		this.insuredMissingHomeAddress = insuredMissingHomeAddress;
	}

	public InsuredDetails getClaimInsuredDetails() {
		return claimInsuredDetails;
	}

	public void setClaimInsuredDetails(InsuredDetails claimInsuredDetails) {
		this.claimInsuredDetails = claimInsuredDetails;
	}
	public int getBusinessSFMCScoreForPrimary() {
		
		return businessSFMCScoreForPrimary;
	}

	public void setBusinessSFMCScoreForPrimary(int businessSFMCScoreForPrimary) {
		this.businessSFMCScoreForPrimary = businessSFMCScoreForPrimary;
	}

	public int getAnalyticSFMCScoreForPrimary() {
		return analyticSFMCScoreForPrimary;
	}

	public void setAnalyticSFMCScoreForPrimary(int analyticSFMCScoreForPrimary) {
		this.analyticSFMCScoreForPrimary = analyticSFMCScoreForPrimary;
	}

	public List<SecondaryImpairment> getSecondaryImpairments() {
		if(secondaryImpairments==null)
			secondaryImpairments=new ArrayList<SecondaryImpairment>();
		return secondaryImpairments;
	}

	public void setSecondaryImpairments(List<SecondaryImpairment> secondaryImpairments) {
		this.secondaryImpairments = secondaryImpairments;
	}

}
