package com.nwm.claims.businessrules.di.fasttrack.model;

public class SecondaryImpairment {
private String secondaryImpairmentCode;
private int analyticSFMCScoreForSecondary;
private int businessSFMCScoreForSecondary;

public SecondaryImpairment() {
	super();
}
public String getSecondaryImpairmentCode() {
	if(secondaryImpairmentCode==null)
		secondaryImpairmentCode="";
	return secondaryImpairmentCode;
}
public void setSecondaryImpairmentCode(String secondaryImpairmentCode) {
	this.secondaryImpairmentCode = secondaryImpairmentCode;
}
public int getAnalyticSFMCScoreForSecondary() {
	return analyticSFMCScoreForSecondary;
}
public void setAnalyticSFMCScoreForSecondary(int analyticSFMCScoreForSecondary) {
	this.analyticSFMCScoreForSecondary = analyticSFMCScoreForSecondary;
}
public int getBusinessSFMCScoreForSecondary() {
	return businessSFMCScoreForSecondary;
}
public void setBusinessSFMCScoreForSecondary(int businessSFMCScoreForSecondary) {
	this.businessSFMCScoreForSecondary = businessSFMCScoreForSecondary;
}

}
